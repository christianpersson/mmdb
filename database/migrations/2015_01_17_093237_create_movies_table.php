<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMoviesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('movies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
            $table->string('imdb_id');
            $table->string('tmdb_id');
            $table->string('vote');
            $table->string('tagline');
            $table->text('overview');
            $table->string('popularity');
            $table->string('poster_path');
            $table->string('release_date');
            $table->string('imdb_rating');
            $table->string('awards');
            $table->string('genre');
            $table->string('runtime');
            $table->string('video_path');
            $table->string('backdrop_path');

            $table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('movies');
	}

}
