<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>MyMovieDataBase</title>

	<link href="{{ url('css/app.css') }}" rel="stylesheet">

    <link href="{{ url('css/player.css') }}" rel="stylesheet">

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="{{ url('css/selectize.bootstrap3.min.css') }}">
    <link rel="stylesheet" href="{{ url('css/select-movie.css') }}">

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

    <style>

        .thumbnail {
            position:relative;
            overflow:hidden;
        }

        .caption {
            position:absolute;
            top:0;
            right:0;
            background:rgba(0, 0, 0, 0.7);
            width:100%;
            height:100%;
            padding:2%;
            display: none;
            text-align:center;
            color:#fff !important;
            z-index:2;
        }
    </style>

</head>

<body>

    @include('navigation')

	@yield('content')

    @include('footer')

    @include('scripts')

</body>
</html>
