@extends('app')

@section('content')
<!-- About Section -->
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">About</h2>

                <h3 class="section-subheading text-muted">My name is Christian. I am a physicist, programmer and a hobby
                    web developer.</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <ul class="timeline">
                    <li>
                        <div class="timeline-image">
                            <img class="img-circle img-responsive" src="img/about/ume.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2010</h4>
                                <h4 class="subheading">My study career in Umeå begins</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">The three first years in the master programme in engineering
                                    physcis was mostly basic courses in mathematics and physics.</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <img class="img-circle img-responsive" src="img/about/fire.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2013</h4>
                                <h4 class="subheading">Physical simulations</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">My interest for physics and programming lead me to study advanced
                                    courses in physical computations and simulations.</p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="timeline-image">
                            <img class="img-circle img-responsive" src="img/about/3.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2014</h4>
                                <h4 class="subheading">Transition to full spare time web developer</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">My studies opened my eyes for programming, which brings me in to
                                    the web developer travel. I began to play with basic stuff. Later I learned
                                    different back-end framwork such as Django and Yii until I finally converged to
                                    Laravel.</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <img class="img-circle img-responsive" src="img/about/network.jpg" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>2015</h4>
                                <h4 class="subheading">Master project and graduation</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">Recently I begin my final project. My project is a research
                                    project about non Markovian networks, @IceLab in Umeå. The project is an exiting mix
                                    between physical models, statistics and programming.</p>
                            </div>
                        </div>
                    </li>
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4>Be Part
                                <br>Of My
                                <br>Story!</h4>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Services Section -->
<section id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Skills</h2>

                <h3 class="section-subheading text-muted">Master degree in engineering physics at Umeå University
                    2015.</h3>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
                    </span>
                <h4 class="service-heading">Physical simulations</h4>

                <p class="text-muted">Monte Carlo methods, Finite Element Methods (Matlab, Comsol), Markov chains,
                    partial differential equations.</p>
            </div>
            <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-globe fa-stack-1x fa-inverse"></i>
                    </span>
                <h4 class="service-heading">Web development</h4>

                <p class="text-muted">Mostly back-end development. Comfortable with HTML, PHP, JavaScript, CSS powered
                    with framework as Laravel and AngularJS.</p>
            </div>
            <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-code fa-stack-1x fa-inverse"></i>
                    </span>
                <h4 class="service-heading">Programming</h4>

                <p class="text-muted">C, C++, Java, Python, Matlab, Julia, Network applications, UI applications.</p>
            </div>
        </div>
    </div>
</section>


@endsection