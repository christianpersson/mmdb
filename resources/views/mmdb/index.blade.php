@extends('app')
@section('content')

    <section id="plot">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Add movie</h1>
                    <div class="demo">
                        <h2>Add movie from TMDB library</h2>
                        <div class="control-group">
                            <label for="select-movie">Movie:</label>
                            <select id="select-movie" class="movies" placeholder="Find a movie..."></select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
