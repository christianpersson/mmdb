@extends('app')

@section('content')

<section id="plot">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Edit movie</h1>
                {{ Html::ul($errors->all()) }}
                {!! Form::model($movie, array('class' => 'form-horizontal', 'role' => 'form', 'method' => 'PUT', 'route' => array('movies.update', $movie->id)))!!}

                @include('movies.form')

                {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                {!!Form::close() !!}

            </div>
        </div>
    </div>
</section>

@endsection

