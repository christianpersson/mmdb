@extends('app')
@section('content')
@foreach($movies as $movie)
@include('movies.partial')
@endforeach

<section>
    <div class="container">
        {!! $movies->render() !!}
    </div>
</section>
@include('movies.player-modal')

@endsection