@extends('app')
@section('content')


<div class="container">
    @foreach($movies->chunk(4) as $movieChunk)
        <div class="row">
        @foreach($movieChunk as $movie)
            @include('movies.thumb')
        @endforeach
        </div>
    @endforeach

    {!! $movies->render() !!}

</div>

@include('movies.player-modal')
@endsection