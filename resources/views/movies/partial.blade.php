<section>
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="thumb">
                    @if($movie->video_path)
                        <a href="#" data-toggle="modal" data-target="#videoModal"
                           data-theVideo="//www.youtube.com/embed/{{$movie->video_path}}">
                            <span class="play">&#9658;</span>
                            <div class="overlay"></div>
                        </a>
                    @else
                    <a href="{{ route('movies.show', $movie->id) }}">
                        <span class="play"></span>
                        <div class="overlay"></div>
                    </a>
                    @endif
                    <img src="{{ @$movie['poster_path'] ?: url('no-poster-w185.jpg') }}" style="height: 200px">
                </div>
            </div>
            <div class="col-md-8">
                <h3>{!! link_to_route('movies.show', $movie->title, $movie->id) !!}
                    <small>({{ $movie->release_date }})</small>
                     
                </h3>
                <h5>{!! $movie->tagline !!}</h5>

                <p>{{ $movie->overview }} </p>

            </div>
            <div class="col-md-2">
                <p>
                <div class="label label-default">{{$movie->vote}}</div>
                </p>
                <p>{{ $movie->runtime }}</p>

                <p>{{ $movie->genre }} </p>

                <p>{{ $movie->awards }} </p>

            </div>
        </div>
        <hr>
    </div>
</section>




