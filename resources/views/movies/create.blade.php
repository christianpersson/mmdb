@extends('app')

@section('content')

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Add movie</h1>
                {{ Html::ul($errors->all()) }}
                {!! Form::open(array('url' => 'movies')) !!}
                @include('movies.form')
                {!! Form::submit('Save', array('class' => 'btn btn-primary')) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</section>

@endsection

