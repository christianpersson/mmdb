<div class="col-md-3">
    <div class="thumbnail">
        <div class="caption">
            <h3>{!! @$movie['title'] ? link_to_route('movies.show', $movie['title'], $movie['id']): 'title' !!}</h3>
            <h4>{{ @$movie['tagline'] ?: '' }}</h4>
            <p>{{ @$movie['overview'] ?substr($movie['overview'],0, 250): '' }}</p>
            <input data-max="10" data-min="1"  type="number" value="{{@$movie['vote'] ?: ''}}" name="your_awesome_parameter" id="some_id" class="rating" data-icon-lib="fa" data-active-icon="fa-heart" data-inactive-icon="fa-heart-o" data-clearable-icon="fa-trash-o"/>{{@$movie['vote'] ?: ''}}
            @if($movie->video_path)
                <a href="#" class="btn btn-link" data-toggle="modal" data-target="#videoModal"
                    data-theVideo="//www.youtube.com/embed/{{$movie->video_path}}"><span class="fa fa-play"></span></a>
            @endif
        </div>
        <img src="{{ @$movie['poster_path'] ?: url('no-poster-w185.jpg') }}" alt="...">


    </div>
</div>