<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>

<script src="{{url('js/jquery.js')}}"></script>
<script src="{{url('js/selectize.min.js')}}"></script>
<script src="{{url('js/selectize-standalone.min.js')}}"></script>

<script src="{{ url('js/select-movie.js') }}"></script>

<script src="{{ url('js/bootstrap-rating-input.min.js') }}"></script>


<script src="{{ url('js/player.js') }}"></script>

<script>
    $( document ).ready(function() {

        $('.thumbnail').hover(
            function(){
                $(this).find('.caption').slideDown(250); //.fadeIn(250)
            },
            function(){
                $(this).find('.caption').slideUp(250); //.fadeOut(205)
            }
        );

    });
</script>