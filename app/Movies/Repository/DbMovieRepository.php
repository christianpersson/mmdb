<?php namespace App\Movies\Repository;

use App\Movies\Movie;
use App\Movies\TMDB;
use App\Repository\DbBaseRepository;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Paginator;

class DbMovieRepository extends DbBaseRepository implements MovieRepositoryInterface
{
    protected $modelClassName = "App\Movies\Movie";

    protected $tmdb;

    public function __construct(){
        $this->tmdb = new TMDB();
    }


    public function updateTmdbData($id){
        $instance = $this->find($id);
        $movie = $this->tmdb->get($instance->tmdb_id);
        $instance->title = $movie['title'];
        $instance->imdb_id = $movie['imdb_id'];
        $instance->vote = $movie['vote_average'];
        $instance->tagline = $movie['tagline'];
        $instance->overview = $movie['overview'];
        $instance->runtime = $movie['runtime'];
        $instance->genre = implode(', ', array_map(
            function($e){
                return $e['name'];
            }, $movie['genres']));

        $instance->popularity = $movie['popularity'];
        $instance->release_date = $movie['release_date'];
        $instance->poster_path = $movie['poster_path'] == "" ? "" : $this->tmdb->image()->getUrl($movie['poster_path'], 'w300');
        $instance->backdrop_path = $movie['backdrop_path'] == "" ? "" : $this->tmdb->image()->getUrl($movie['backdrop_path'], 'original');

        $instance->save();
        return $instance;
    }

    public function updateImdbData($id){
        $instance = $this->find($id);
        $json=file_get_contents("http://www.omdbapi.com/?i=".$instance->imdb_id);
        $info=json_decode($json);
        $instance->imdb_rating = $info->imdbRating;
        $instance->awards = $info->Awards;
        $instance->save();
    }


    public function updateVideoData($id){
        $instance = $this->find($id);
        $videos = $this->tmdb->getVideos($instance->tmdb_id)['results'];
        if(count($videos)){
            $instance->video_path = $videos[0]['key'];
            $instance->save();
        }
    }

}