<?php namespace App\Movies\Repository;

use \App\Repository\BaseRepository;

interface MovieRepositoryInterface extends BaseRepository{

    public function updateTmdbData($id);

    public function updateImdbData($id);

    public function updateVideoData($id);

} 