<?php namespace App\Movies;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'movies';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['title', 'tmdb_id', 'imdb_id', 'imdb_rating', 'release_date', 'poster_path'];

}
