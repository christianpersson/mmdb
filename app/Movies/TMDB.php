<?php
/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 17/01/15
 * Time: 14:55
 */

namespace App\Movies;

use Tmdb\Model\Collection;
use Tmdb\Repository\FindRepository;
use Tmdb\Repository\MovieRepository;


class TMDB {

    private $client;
    private $config;

    public function __construct(){
        $token  = new \Tmdb\ApiToken('933d37a7453e5fba6ab75a2a56d28f5c');
        $this->client = new \Tmdb\Client($token);

        $configRepository = new \Tmdb\Repository\ConfigurationRepository($this->client);
        $this->config = $configRepository->load();

    }


    /**
     * @param $q
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function search($q){
        $query = new \Tmdb\Model\Search\SearchQuery\MovieSearchQuery();
        $repository = new \Tmdb\Repository\SearchRepository($this->client);

        $find = $repository->searchMovie($q, $query)->toArray();
        return \Illuminate\Database\Eloquent\Collection::make($find)->sortBy(function(\Tmdb\Model\Movie $movie){
            return -$movie->getPopularity();
        });
    }



    /**
     * @param $id
     * @param string $lang
     * @return mixed
     */
    public function get($id, $lang = 'en'){
        return $this->getApi()->getMovie($id, ['language' => $lang]);
    }

    /**
     * @param $id
     * @return \Tmdb\Model\Find
     */
    public function find($id){
        $repo = new FindRepository($this->client);
        return $repo->find($id, array(
            'external_source' => 'imdb_id'
        ));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getCredits($id){
        return $this->getApi()->getCredits($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getImages($id){
        return $this->getApi()->getImages($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getVideos($id){
        return $this->getApi()->getVideos($id);
    }

    /**
     * @return \Tmdb\Api\Movies
     */
    public function getApi(){
        $repo = new MovieRepository($this->client);
        return $repo->getApi();
    }


    /**
     * @return \Tmdb\Helper\ImageHelper
     */
    public function image(){
        return  new \Tmdb\Helper\ImageHelper($this->config);
    }

} 