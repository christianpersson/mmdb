<?php namespace App\Console\Commands;

use App\Movies\Movie;
use App\Movies\Repository\DbMovieRepository;
use App\Movies\TMDB;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CrawlTMDB extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'crawl';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Display an inspiring quote';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
        $tmdb = new TMDB();
        $repo = new DbMovieRepository();

        $myfile = fopen($this->argument('example'), "r") or die("Unable to open file!");
        $i = 0;
        while(!feof($myfile)) {

            $imdb_id = preg_replace('~[\r\n]+~', '', fgets($myfile));
            if($imdb_id == "")
                continue;
            if($instance = Movie::all()->where('imdb_id', $imdb_id)->first()){
                echo "Already added: " . $imdb_id . " " . $instance->title . " " . $i++ ."\n";
            }
            else{
                if(($movie = $tmdb->find($imdb_id)->getMovieResults()->getIterator()->current()) != null){
                    $id = $movie->getId();
                    $instance = $repo->create(['tmdb_id' => $id, 'id' => $id]);
                    $instance = $repo->updateTmdbData($instance->id);
                    echo "Added: " . $imdb_id . " " . $instance->title . " " . $i++ ."\n";
                }
                else{
                    $instance = $repo->create(['imdb_id' => $imdb_id]);
                    echo "--Fail to add: " . $imdb_id . " " . $i++ ."\n";
                }
            }


        }
        fclose($myfile);
	}

    protected function getArguments()
    {
        return array(
            array('example', InputArgument::REQUIRED, 'An example argument.'),
        );
    }

}
