<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);


Route::get('movie', function(){
    $s = new \App\Movies\TMDB();
    $s->search('star wars')->each(function(\Tmdb\Model\Movie $movie){
        echo "<li>"
            . $movie->getTitle()
            . " - " . $movie->getPopularity()
            . " - " . $movie->getId()
            . "</li>";
    });
    dd($s->get(11));
});


Route::get('mmdb', function(){
    return view('mmdb.app');
});

Route::get('popular',  ['uses' => 'MoviesController@popular', 'as' => 'movies.popular']);
Route::get('top',  ['uses' => 'MoviesController@top', 'as' => 'movies.top']);
Route::get('upcoming',  ['uses' => 'MoviesController@upcoming', 'as' => 'movies.upcoming']);

Route::get('movies/search', ['uses' => 'MoviesController@search', 'as' => 'movies.search']);
Route::get('movies/poster', ['uses' => 'MoviesController@all', 'as' => 'movies.poster']);
Route::resource('movies', 'MoviesController');

