<?php namespace App\Http\Controllers;



use App\Movies\Repository\MovieRepositoryInterface;
use App\Movies\TMDB;
use App\User;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Tmdb\Model\Movie;

class MoviesController extends CrudController {

    private $tmdb;
    public function __construct(MovieRepositoryInterface $repo)
    {
        parent::__construct($repo, null, 'movie', 'Filmer');
    }


    public function index($data = [])
    {
        $instances = $this->repo->paginate(10);
        $instances->setPath(route('movies.index'));

        return view("{$this->route}.index",[
                $this->route => $instances,
                'links' => $this->editLinks('index')
            ] + $data);
    }


    public function show($id, $data = [])
    {
        $instance = $this->repo->find($id);
        if($instance->awards == null){
            $this->repo->updateImdbData($id);
        }
        if($instance->video_path == null){
            $this->repo->updateVideoData($id);
        }
        return parent::show($id, $data);
    }

    public function store(){
        $instance = $this->storePartial();
        $instance->tmdb_id = Input::get('tmdb_id');
        $this->repo->updateTmdbData($instance->id);
        $this->repo->updateImdbData($instance->id);
        return $this->redirect($instance);
    }

    public function search(TMDB $repo){
        $q = Input::get('q');
        $data = $repo->search($q)->map(function(Movie $d) use ($repo){
            return [
                'title' => $d->getTitle(),
                'id' => $d->getId(),
                'year' => Carbon::createFromTimestamp($d->getReleaseDate()->getTimestamp())->year,
                'vote' => $d->getVoteAverage()
            ];
        });

        return Response::json(compact('data'));
    }

    private function preview($a, $title){
        $movies = Collection::make($a)->map(function($d){
            $obj = \App\Movies\Movie::where('tmdb_id', '=', $d['id'])->first();
            if($obj == null){
                $obj = \App\Movies\Movie::create([
                        'tmdb_id' => $d['id'],
                        'id' => $d['id']
                    ]
                );
                $this->repo->updateTmdbData($obj->id);
            }
            return $obj;
        });
        return view("movies.preview", compact('movies', 'title'));
    }

    public function all(){
        $movies = $this->repo->paginate(16);
        $movies->setPath(route('movies.poster'));

        $title = 'All movies';
        return view("movies.preview", compact('movies', 'title'));
    }

    public function latest(TMDB $repo){
        return $this->all();
        return $this->upcoming($repo);
    }

    public function upcoming(TMDB $repo){
        return $this->all();
        return $this->preview($repo->getApi()->getUpcoming()['results'], 'Upcoming movies');
    }

    public function popular(TMDB $repo){
        return $this->all();
        return $this->preview($repo->getApi()->getPopular()['results'], 'Popular movies');
    }

    public function top(TMDB $repo){
        return $this->all();
        return $this->preview($repo->getApi()->getTopRated()['results'], 'Top rated movies');
    }
}
