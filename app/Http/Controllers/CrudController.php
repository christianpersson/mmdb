<?php namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;


/**
 * Created by PhpStorm.
 * User: christianpersson
 * Date: 11/08/14
 * Time: 22:05
 */



abstract class CrudController extends BaseController
{
    /** @var  BaseRepository */
    protected $repo;

    /** @var  FormValidator */
    protected $form;

    protected $model;
    protected $modelTitle;
    protected $route;
    protected $messages;
    protected $buttonCreator;


    //TODO: add modelname and modelTitle to constructor to remove setModelName method and to breadcrumbs
    function __construct($repo, $form = null, $modelName, $modelTitle, $route = null)
    {
        parent::__construct();
        $this->repo = $repo;
        $this->form = $form;
        $this->model = $modelName;
        $this->modelTitle = $modelTitle;

        if($route == null){
            $this->route = str_plural($this->model);
        }else{
            $this->route = $route;
        }

        $this->setMessages();

    }

    public function index($data = [])
    {
        $instances = $this->repo->all();

        return view("{$this->route}.index",[
            $this->route => $instances,
            'links' => $this->editLinks('index')
        ] + $data);
    }


    public function show($id, $data = [])
    {
        $instance = $this->repo->find($id);

        return view("{$this->route}.show",
            [
                $this->model => $instance,
                'links' => $this->editLinks('show', $id)
            ] + $data);
    }


    public function create()
    {

        return view("{$this->route}.create")->with(static::formData() +
            ['links'=> $this->editLinks('create')]
        );
    }


    public function edit($id)
    {
        $instance = $this->repo->find($id);

        return view("{$this->route}.edit",
            [$this->model => $instance])->with(static::formData() +
                ['links'=> $this->editLinks('edit', $id)]
            );
    }


    public function store()
    {
        $instance = $this->storePartial();

        return $this->redirect($instance);
    }


    public function storePartial($input = null)
    {
        if($input == null){
            $input = Input::all();
        }

        $instance = $this->repo->create($input);

        $this->updateRelations($instance);

        return $instance;
    }


    public function update($id)
    {
        $instance = $this->updatePartial($id);

        return $this->redirect($instance);
    }

    public function updatePartial($id){

        $input = Input::all();

        $instance = $this->repo->find($id);

        $instance = $this->repo->updateWithIdAndInput($id, $input);

        $this->updateRelations($instance);

        return $instance;
    }

    /**
     * Destroy a model instance
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $this->repo->destroy($id);

        return Redirect::back();
    }

    /**
     * Set a default flash messages
     * Should be overridden in sub-controller.
     */
    protected function setMessages()
    {
        $this->messages = [
            'created' => $this->model . ' tillagd.',
            'updated' => $this->model . ' uppdaterad.',
            'deleted' => $this->model . ' borttagen.'
        ];
    }

    /**
     * Update relation to instance.
     * This method must be overridden in caller class.
     *
     * @param $instance
     */
    protected function updateRelations($instance){}

    protected function redirect($instance){
        return Redirect::route("{$this->route}.show", $instance->id);
    }

    protected function formData(){
        return [];
    }

    /**
     * Help function for show crud buttons in views.
     *
     * @param string $route
     * @param null $id
     * @return mixed
     */
    protected function editLinks($route = 'index', $id = null){
       return "";
    }


    protected function getAttribute($key, $array, $default = []){
        if (array_key_exists($key, $array)) {
            return $array[$key];
        }
        return $default;
    }

} 