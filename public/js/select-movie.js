/**
 * Created by christianpersson on 18/01/15.
 */
$(document).ready(function(){
    $('#select-movie').selectize({
        valueField: 'id',
        labelField: 'title',
        searchField: 'title',
        options: [],
        render: {
            option: function(item, escape) {
                return '<div>' +
                    '<span class="title">' +
                    '<span class="name">' + escape(item.title) + ' (' + escape(item.year) + ') - ' + escape(item.vote) + '</span>' +
                    '</span>' +
                    '</div>';
            }
        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: "/mmdb/public/movies/search",
                type: 'GET',
                dataType: 'json',
                data: {
                    q: query
                },
                error: function() {
                    callback();
                },
                success: function(res) {
                    console.log(res.data);
                    callback(res.data);
                }
            });
        }
    });
});